from trytond.wizard import Wizard, StateView, Button, StateTransition, StateAction
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.pool import Pool 
from trytond.pyson import Eval, Not, Bool, Equal

import csv
import sys
from io import StringIO
from datetime import datetime, date, time, timedelta

__all__ = ['CreateNursingReportsStart',
           'CreateNursingReportsWizard']
# modelview es de vista


class CreateNursingReportsStart(ModelView):    
    'Nursing Report Start'
    __name__ = 'nursing.reports.start'
    
    report_ = fields.Selection([
        ('create_nursing_report','Reporte Estadistico Enfermeria'),
         ],'Report',required=True,sort=False)
    start_date = fields.DateTime('Start Date', required=True)
    end_date = fields.DateTime('End Date', required=True)

    @staticmethod
    def default_report_():
        return 'create_nursing_report'


class CreateNursingReportsWizard(Wizard):
    'Nursing Report Wizard'
    __name__ = 'nursing.reports.wizard'
    
    @classmethod
    def __setup__(cls):
        super(CreateNursingReportsWizard,cls).__setup__()
        cls._error_messages.update({
            'end_date_before_start_date': 'The end date cannot be major thant the start date',
            })
    
    start = StateView('nursing.reports.start',
                      'health_nursing_fiuner.create_nursing_report_start_view',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Print Report','prevalidate','tryton-ok',default=True),
                       ])
    
    prevalidate = StateTransition()
    
    create_nursing_report =\
        StateAction('health_nursing_fiuner.act_gnuhealth_nursing_report')
    
    
    def default_start(self,fields):
        return{
            'end_date':datetime.now()
            }
    
    def transition_prevalidate(self):
        if self.start.end_date < self.start.start_date:
            self.raise_user_error('end_date_before_start_date')
        return self.start.report_

    def fill_data(self):
        start = self.start.start_date
        end = self.start.end_date
        return {
            'start':start, 
            'end':end 
            }
        
    def do_create_nursing_report(self, action):
        data = self.fill_data()
        return action, data
    
