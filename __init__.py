from trytond.pool import Pool
from .health_nursing import *
from .report import *
from .wizard import *

def register():
    Pool.register(
        AmbulatoryCare,
        NProcedures,
        NursingProcedures,
        CreateNursingReportsStart,      
        module='health_nursing_fiuner', type_='model') 
    Pool.register(
        NursingReport,
        module='health_nursing_fiuner', type_='report')
    Pool.register(
        CreateNursingReportsWizard,
        module='health_nursing_fiuner', type_='wizard')
