from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

 
__all__ = ['AmbulatoryCare','NProcedures','NursingProcedures']


class AmbulatoryCare(metaclass = PoolMeta):
    'Ambulatory Care'
    __name__ = 'gnuhealth.patient.ambulatory_care'
    
    STATES = {'readonly': Eval('state') == 'done'}
    
    weight = fields.Float('Weight',states = STATES)    
    height = fields.Float('Height', states = STATES)
    cephalic_perimeter = fields.Float('Cephalic Perimeter', states = STATES)    
    nursing_procedures = fields.Many2Many(
        'gnuhealth.nursing_procedure', 'name', 'n_procedure',
        'Procedures', states = STATES)

    bmi = fields.Float('BMI',states = STATES)
    
    @fields.depends('weight','height')
    def on_change_with_bmi(self):
        bmi = 0
        if self.weight and self.height:
            bmi = float(self.weight) / pow((float(self.height)/100),2)
        return float(bmi)

class NProcedures(ModelSQL,ModelView):
    'Nursing Procedures'
    __name__='gnuhealth.n_procedure'
    
    code = fields.Char('Nursing Procedure Code', required=True)
    proc = fields.Char('Nursing Procedure')


class NursingProcedures(ModelSQL,ModelView):
    ' Procedures'
    __name__='gnuhealth.nursing_procedure'
        
    name = fields.Many2One('gnuhealth.patient.ambulatory_care', 'Session', ondelete='CASCADE')
    n_procedure = fields.Many2One('gnuhealth.n_procedure', 'Procedures', ondelete='CASCADE')
